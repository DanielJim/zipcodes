package cache;

import com.prueba.zip.cache.LoadCache;
import com.prueba.zip.dto.ZipCodeDetails;
import com.prueba.zip.repository.CallDataBase;
import com.prueba.zip.repository.dto.ZipCodesTable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class LoadCacheTest {

    @Mock
    public CallDataBase callDataBase;
    public LoadCache loadCache;

    private static final String key1 = "1-1200";
    private static final String key2 = "2-1200";

    @Test
    public void loadCache_shouldReturnCache_whenOk() {

        Mockito.when(callDataBase.findAll()).thenReturn(dummyResults());
        loadCache = new LoadCache(callDataBase);
        final Map<String, ZipCodeDetails> cache = loadCache.getCache();
        assertNotNull(cache);
        assertEquals("Zona Centro", cache.get(key1).getdAsenta());
        assertEquals("Colinas del Rio", cache.get(key2).getdAsenta());
        Mockito.verify(callDataBase).findAll();
    }

    private List<ZipCodesTable> dummyResults() {

        ZipCodesTable zipCodesTable = new ZipCodesTable();
        zipCodesTable.setId(1);
        zipCodesTable.setdCodigo(1200);
        zipCodesTable.setdAsenta("Zona Centro");
        zipCodesTable.setdTipoAsenta("Colonia");
        zipCodesTable.setdMnpio("Aguascalientes");
        zipCodesTable.setdEstado("Aguascalientes");
        zipCodesTable.setdCiudad("Aguascalientes");
        zipCodesTable.setdCp(20001);
        zipCodesTable.setcEstado("1");
        zipCodesTable.setcOficina(20001);
        zipCodesTable.setcCp("");
        zipCodesTable.setcTipoAsenta("9");
        zipCodesTable.setcMnpio("2");
        zipCodesTable.setIdAsentaCpCons("1");
        zipCodesTable.setdZona("Urbano");
        zipCodesTable.setcCveCiudad("1");

        ZipCodesTable zipCodesTable1 = new ZipCodesTable();
        zipCodesTable1.setId(2);
        zipCodesTable1.setdCodigo(1200);
        zipCodesTable1.setdAsenta("Colinas del Rio");
        zipCodesTable1.setdTipoAsenta("Colonia");
        zipCodesTable1.setdMnpio("Aguascalientes");
        zipCodesTable1.setdEstado("Aguascalientes");
        zipCodesTable1.setdCiudad("Aguascalientes");
        zipCodesTable1.setdCp(20001);
        zipCodesTable1.setcEstado("1");
        zipCodesTable1.setcOficina(20001);
        zipCodesTable1.setcCp("");
        zipCodesTable1.setcTipoAsenta("9");
        zipCodesTable1.setcMnpio("2");
        zipCodesTable1.setIdAsentaCpCons("1");
        zipCodesTable1.setdZona("Urbano");
        zipCodesTable1.setcCveCiudad("1");

        return Arrays.asList(zipCodesTable, zipCodesTable1);
    }
}