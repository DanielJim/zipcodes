package operation;

import com.prueba.zip.cache.LoadCache;
import com.prueba.zip.dto.ZipCodeDetails;
import com.prueba.zip.dto.ZipCodeResults;
import com.prueba.zip.operation.GetZipCodeDetails;
import com.prueba.zip.repository.CallDataBase;
import com.prueba.zip.repository.dto.ZipCodesTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetZipCodeDetailsTest {

    private GetZipCodeDetails getZipCodeDetails;

    @Mock
    public LoadCache loadCache;

    @Mock
    public CallDataBase callDataBase;

    private static final int ZIP_CODE = 1200;
    private static final int ZIP_CODE_1 = 1300;

    @BeforeEach
    public void setUp() {

        getZipCodeDetails = new GetZipCodeDetails(loadCache, callDataBase);
    }

    @Test
    public void getZipCodeDetails_shouldReturnZipCodes_whenOk() {

        when(loadCache.getCache()).thenReturn(dummyMap());
        final ZipCodeResults zipCodeResults = getZipCodeDetails.getZipCode(String.valueOf(ZIP_CODE));
        assertNotNull(zipCodeResults);
    }

    @Test
    public void getZipCodeDetails_shouldReturnZipCodes_whenZipCodeFoundOnDataBase() {
        when(loadCache.getCache()).thenReturn(dummyMap());
        when(callDataBase.findZipCodeOnDataBase(eq(ZIP_CODE_1))).thenReturn(dummyResults());
        final ZipCodeResults zipCodeResults = getZipCodeDetails.getZipCode(String.valueOf(ZIP_CODE_1));
        verify(loadCache).getCache();
        verify(callDataBase).findZipCodeOnDataBase(eq(ZIP_CODE_1));
        assertNotNull(zipCodeResults);
        assertNotNull(zipCodeResults.getListZipCodeDetails());
        assertFalse(zipCodeResults.getListZipCodeDetails().isEmpty());
        assertEquals("Zona Centro", zipCodeResults.getListZipCodeDetails().get(0).getdAsenta());
        assertEquals("Colonia", zipCodeResults.getListZipCodeDetails().get(0).getdTipoAsenta());
        assertNotNull(zipCodeResults);
    }

    @Test
    public void getZipCodeDetails_shouldReturnException_whenNoZipCodesFoundOnDataBase() {
        when(loadCache.getCache()).thenReturn(dummyMap());
        when(callDataBase.findZipCodeOnDataBase(eq(ZIP_CODE_1))).thenReturn(Collections.emptyList());
        assertThrows(RuntimeException.class, () -> getZipCodeDetails.getZipCode(String.valueOf(ZIP_CODE_1)));
    }

    private Map<String, ZipCodeDetails> dummyMap() {

        Map<String, ZipCodeDetails> zipCodeDetails = new HashMap<>();
        zipCodeDetails.put("1-1200", new ZipCodeDetails().setdCodigo("1200")
                .setdAsenta("Asenta"));
        zipCodeDetails.put("2-1200", new ZipCodeDetails().setdCodigo("1200")
                .setdAsenta("Asenta1"));
        zipCodeDetails.put("3-1201", new ZipCodeDetails().setdCodigo("1201")
                .setdAsenta("Asenta3"));
        return zipCodeDetails;
    }

    private List<ZipCodesTable> dummyResults() {

        ZipCodesTable zipCodesTable = new ZipCodesTable();
        zipCodesTable.setId(1);
        zipCodesTable.setdCodigo(1300);
        zipCodesTable.setdAsenta("Zona Centro");
        zipCodesTable.setdTipoAsenta("Colonia");
        zipCodesTable.setdMnpio("Aguascalientes");
        zipCodesTable.setdEstado("Aguascalientes");
        zipCodesTable.setdCiudad("Aguascalientes");
        zipCodesTable.setdCp(20001);
        zipCodesTable.setcEstado("1");
        zipCodesTable.setcOficina(20001);
        zipCodesTable.setcCp("");
        zipCodesTable.setcTipoAsenta("9");
        zipCodesTable.setcMnpio("2");
        zipCodesTable.setIdAsentaCpCons("1");
        zipCodesTable.setdZona("Urbano");
        zipCodesTable.setcCveCiudad("1");

        return Collections.singletonList(zipCodesTable);
    }
}