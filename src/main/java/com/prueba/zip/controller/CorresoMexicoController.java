package com.prueba.zip.controller;

import com.prueba.zip.dto.ZipCodeResults;
import com.prueba.zip.operation.GetZipCodeDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CorresoMexicoController {
    private final GetZipCodeDetails getZipCodeDetails;

    @Autowired
    public CorresoMexicoController(GetZipCodeDetails getZipCodeDetails) {
        this.getZipCodeDetails = getZipCodeDetails;
    }

    @GetMapping(value = "/zip-codes/{zip-code}",
            produces = {"application/json"})
    public ZipCodeResults getZipCodesDetails(@PathVariable("zip-code") String zipCode) {


        return getZipCodeDetails.getZipCode(zipCode);
    }
}