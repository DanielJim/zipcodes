package com.prueba.zip.cache;

import com.prueba.zip.dto.ZipCodeDetails;
import com.prueba.zip.repository.CallDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LoadCache {

    private final CallDataBase callDatabase;
    public final Map<String, ZipCodeDetails> cache;

    @Autowired
    public LoadCache(CallDataBase callDatabase) {

        this.callDatabase = callDatabase;
        this.cache = loadCache();
    }

    private Map<String, ZipCodeDetails> loadCache() {

        final Map<String, ZipCodeDetails> mapResults = new HashMap<>();
        callDatabase.findAll()
                .forEach(results -> mapResults.put(new StringBuilder().append(results.getId())
                        .append("-")
                        .append(results.getdCodigo())
                        .toString(), new ZipCodeDetails()
                        .setdCodigo(String.valueOf(results.getdCodigo()))
                        .setdAsenta(results.getdAsenta())
                        .setdTipoAsenta(results.getdTipoAsenta())
                        .setdMnpio(results.getdMnpio())
                        .setdEstado(results.getdEstado())
                        .setdCiudad(results.getdCiudad())
                        .setdCp(String.valueOf(results.getdCp()))
                        .setcEstado(results.getcEstado())
                        .setcOficina(String.valueOf(results.getcOficina()))
                        .setcCp(results.getcCp())
                        .setcTipoAsenta(results.getcTipoAsenta())
                        .setcMnpio(results.getcMnpio())
                        .setIdAsenta(results.getIdAsentaCpCons())
                        .setdZona(results.getdZona())
                        .setcCveCiudad(results.getcCveCiudad())));
        return mapResults;
    }

    public Map<String, ZipCodeDetails> getCache() {
        return cache;
    }
}