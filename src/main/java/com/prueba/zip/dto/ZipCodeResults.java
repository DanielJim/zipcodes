package com.prueba.zip.dto;

import java.util.List;

public class ZipCodeResults {

    private List<ZipCodeDetails> listZipCodeDetails;

    public List<ZipCodeDetails> getListZipCodeDetails() {
        return listZipCodeDetails;
    }

    public ZipCodeResults setListZipCodeDetails(List<ZipCodeDetails> listZipCodeDetails) {
        this.listZipCodeDetails = listZipCodeDetails;
        return this;
    }
}