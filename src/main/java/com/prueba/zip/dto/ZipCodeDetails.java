package com.prueba.zip.dto;

public class ZipCodeDetails {

    private String dCodigo;
    private String dAsenta;
    private String dTipoAsenta;
    private String dMnpio;
    private String dEstado;
    private String dCiudad;
    private String dCp;
    private String cEstado;
    private String cOficina;
    private String cCp;
    private String cTipoAsenta;
    private String cMnpio;
    private String idAsenta;
    private String dZona;
    private String cCveCiudad;

    public String getdCodigo() {
        return dCodigo;
    }

    public ZipCodeDetails setdCodigo(String dCodigo) {
        this.dCodigo = dCodigo;
        return this;
    }

    public String getdAsenta() {
        return dAsenta;
    }

    public ZipCodeDetails setdAsenta(String dAsenta) {
        this.dAsenta = dAsenta;
        return this;
    }

    public String getdTipoAsenta() {
        return dTipoAsenta;
    }

    public ZipCodeDetails setdTipoAsenta(String dTipoAsenta) {
        this.dTipoAsenta = dTipoAsenta;
        return this;
    }

    public String getdMnpio() {
        return dMnpio;
    }

    public ZipCodeDetails setdMnpio(String dMnpio) {
        this.dMnpio = dMnpio;
        return this;
    }

    public String getdEstado() {
        return dEstado;
    }

    public ZipCodeDetails setdEstado(String dEstado) {
        this.dEstado = dEstado;
        return this;
    }

    public String getdCiudad() {
        return dCiudad;
    }

    public ZipCodeDetails setdCiudad(String dCiudad) {
        this.dCiudad = dCiudad;
        return this;
    }

    public String getdCp() {
        return dCp;
    }

    public ZipCodeDetails setdCp(String dCp) {
        this.dCp = dCp;
        return this;
    }

    public String getcEstado() {
        return cEstado;
    }

    public ZipCodeDetails setcEstado(String cEstado) {
        this.cEstado = cEstado;
        return this;
    }

    public String getcOficina() {
        return cOficina;
    }

    public ZipCodeDetails setcOficina(String cOficina) {
        this.cOficina = cOficina;
        return this;
    }

    public String getcCp() {
        return cCp;
    }

    public ZipCodeDetails setcCp(String cCp) {
        this.cCp = cCp;
        return this;
    }

    public String getcTipoAsenta() {
        return cTipoAsenta;
    }

    public ZipCodeDetails setcTipoAsenta(String cTipoAsenta) {
        this.cTipoAsenta = cTipoAsenta;
        return this;
    }

    public String getcMnpio() {
        return cMnpio;
    }

    public ZipCodeDetails setcMnpio(String cMnpio) {
        this.cMnpio = cMnpio;
        return this;
    }

    public String getIdAsenta() {
        return idAsenta;
    }

    public ZipCodeDetails setIdAsenta(String idAsenta) {
        this.idAsenta = idAsenta;
        return this;
    }

    public String getdZona() {
        return dZona;
    }

    public ZipCodeDetails setdZona(String dZona) {
        this.dZona = dZona;
        return this;
    }

    public String getcCveCiudad() {
        return cCveCiudad;
    }

    public ZipCodeDetails setcCveCiudad(String cCveCiudad) {
        this.cCveCiudad = cCveCiudad;
        return this;
    }
}