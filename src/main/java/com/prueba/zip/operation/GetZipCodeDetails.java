package com.prueba.zip.operation;

import com.prueba.zip.cache.LoadCache;
import com.prueba.zip.dto.ZipCodeDetails;
import com.prueba.zip.dto.ZipCodeResults;
import com.prueba.zip.repository.CallDataBase;
import exceptions.ZipNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GetZipCodeDetails {

    private final LoadCache loadCache;
    private final CallDataBase callDataBase;

    @Autowired
    public GetZipCodeDetails(LoadCache loadCache, CallDataBase callDataBase) {

        this.loadCache = loadCache;
        this.callDataBase = callDataBase;
    }

    public ZipCodeResults getZipCode(final String zipCode) {

        final List<ZipCodeDetails> listZipCodeDetails = new ArrayList<>();
        loadCache.getCache()
                .forEach((k, v) -> {
                    if (zipCode.equals(k.substring(k.indexOf("-") + 1)))
                        listZipCodeDetails.add(v);
                });

        if (listZipCodeDetails.isEmpty()) {
            return new ZipCodeResults().setListZipCodeDetails(Optional.of(callDataBase.findZipCodeOnDataBase(Integer.parseInt(zipCode))
                    .stream()
                    .map(results -> new ZipCodeDetails()
                            .setdCodigo(String.valueOf(results.getdCodigo()))
                            .setdAsenta(results.getdAsenta())
                            .setdTipoAsenta(results.getdTipoAsenta())
                            .setdMnpio(results.getdMnpio())
                            .setdEstado(results.getdEstado())
                            .setdCiudad(results.getdCiudad())
                            .setdCp(String.valueOf(results.getdCp()))
                            .setcEstado(results.getcEstado())
                            .setcOficina(String.valueOf(results.getcOficina()))
                            .setcCp(results.getcCp())
                            .setcTipoAsenta(results.getcTipoAsenta())
                            .setcMnpio(results.getcMnpio())
                            .setIdAsenta(results.getIdAsentaCpCons())
                            .setdZona(results.getdZona())
                            .setcCveCiudad(results.getcCveCiudad()))
                    .collect(Collectors.toList()))
                    .filter(listZipCodes -> !listZipCodes.isEmpty())
                    .orElseThrow(ZipNotFoundException::new));
        } else {
            return new ZipCodeResults().setListZipCodeDetails(listZipCodeDetails);
        }
    }
}