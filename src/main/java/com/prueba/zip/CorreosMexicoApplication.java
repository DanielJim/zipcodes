package com.prueba.zip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.prueba.zip"})
public class CorreosMexicoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CorreosMexicoApplication.class, args);
    }
}