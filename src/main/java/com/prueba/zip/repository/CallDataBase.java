package com.prueba.zip.repository;

import com.prueba.zip.repository.dto.ZipCodesTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CallDataBase extends JpaRepository<ZipCodesTable, Long> {

    @Query("select zips from ZipCodesTable zips where zips.dCodigo = :dCodigo")
    List<ZipCodesTable> findZipCodeOnDataBase(@Param("dCodigo") int dCodigo);
}