package com.prueba.zip.repository.dto;

import javax.persistence.*;

@Entity
@Table(name = "ZIP_CODES")
public class ZipCodesTable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "d_codigo")
    private int dCodigo;
    @Column(name = "d_asenta")
    private String dAsenta;
    @Column(name = "d_tipo_asenta")
    private String dTipoAsenta;
    @Column(name = "d_mnpio")
    private String dMnpio;
    @Column(name = "d_estado")
    private String dEstado;
    @Column(name = "d_ciudad")
    private String dCiudad;
    @Column(name = "d_cp")
    private int dCp;
    @Column(name = "c_estado")
    private String cEstado;
    @Column(name = "c_oficina")
    private int cOficina;
    @Column(name = "c_cp")
    private String cCp;
    @Column(name = "c_tipo_asenta")
    private String cTipoAsenta;
    @Column(name = "c_mnpio")
    private String cMnpio;
    @Column(name = "id_asenta_cpcons")
    private String idAsentaCpCons;
    @Column(name = "d_zona")
    private String dZona;
    @Column(name = "c_cve_ciudad")
    private String cCveCiudad;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getdCodigo() {
        return dCodigo;
    }

    public void setdCodigo(int dCodigo) {
        this.dCodigo = dCodigo;
    }

    public String getdAsenta() {
        return dAsenta;
    }

    public void setdAsenta(String dAsenta) {
        this.dAsenta = dAsenta;
    }

    public String getdTipoAsenta() {
        return dTipoAsenta;
    }

    public void setdTipoAsenta(String dTipoAsenta) {
        this.dTipoAsenta = dTipoAsenta;
    }

    public String getdMnpio() {
        return dMnpio;
    }

    public void setdMnpio(String dMnpio) {
        this.dMnpio = dMnpio;
    }

    public String getdEstado() {
        return dEstado;
    }

    public void setdEstado(String dEstado) {
        this.dEstado = dEstado;
    }

    public String getdCiudad() {
        return dCiudad;
    }

    public void setdCiudad(String dCiudad) {
        this.dCiudad = dCiudad;
    }

    public int getdCp() {
        return dCp;
    }

    public void setdCp(int dCp) {
        this.dCp = dCp;
    }

    public String getcEstado() {
        return cEstado;
    }

    public void setcEstado(String cEstado) {
        this.cEstado = cEstado;
    }

    public int getcOficina() {
        return cOficina;
    }

    public void setcOficina(int cOficina) {
        this.cOficina = cOficina;
    }

    public String getcCp() {
        return cCp;
    }

    public void setcCp(String cCp) {
        this.cCp = cCp;
    }

    public String getcTipoAsenta() {
        return cTipoAsenta;
    }

    public void setcTipoAsenta(String cTipoAsenta) {
        this.cTipoAsenta = cTipoAsenta;
    }

    public String getcMnpio() {
        return cMnpio;
    }

    public void setcMnpio(String cMnpio) {
        this.cMnpio = cMnpio;
    }

    public String getIdAsentaCpCons() {
        return idAsentaCpCons;
    }

    public void setIdAsentaCpCons(String idAsentaCpCons) {
        this.idAsentaCpCons = idAsentaCpCons;
    }

    public String getdZona() {
        return dZona;
    }

    public void setdZona(String dZona) {
        this.dZona = dZona;
    }

    public String getcCveCiudad() {
        return cCveCiudad;
    }

    public void setcCveCiudad(String cCveCiudad) {
        this.cCveCiudad = cCveCiudad;
    }
}